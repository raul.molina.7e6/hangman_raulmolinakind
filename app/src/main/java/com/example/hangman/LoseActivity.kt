package com.example.hangman

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.hangman.databinding.ActivityLoseBinding
import com.example.hangman.databinding.ActivityMainBinding

class LoseActivity : AppCompatActivity() {
    lateinit var binding: ActivityLoseBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        //QUITAR BARRA DE ARRIBA
        if(supportActionBar != null){
            supportActionBar?.hide()
        }

        super.onCreate(savedInstanceState)
        binding = ActivityLoseBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        val ganadoOperdido = bundle?.getString("ganadoOperdido")
        val palabraSecreta = bundle?.getString("palabraSecreta")

        if(ganadoOperdido == "perdido"){
            binding.textoPerder.visibility = View.VISIBLE
            binding.textocuandopierdes.text = "La palabra era $palabraSecreta"
            binding.textocuandopierdes.visibility = View.VISIBLE
        }else{
            binding.textoGanar.visibility = View.VISIBLE
        }


        binding.menuPrincipal.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}

