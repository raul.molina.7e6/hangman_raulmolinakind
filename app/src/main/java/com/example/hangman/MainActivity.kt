package com.example.hangman

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hangman.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        //QUITAR BARRA DE ARRIBA
        if(supportActionBar != null){
            supportActionBar?.hide()
        }

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //BOTON DE PLAY
        binding.botonJugar.setOnClickListener {
            val dificultad = binding.dificultades.selectedItem.toString()
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("dificultad", dificultad)
            startActivity(intent)
        }


    }
}

class ActivityMainBinding {

}
