package com.example.hangman

import android.annotation.SuppressLint
import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.view.isVisible
import com.example.hangman.databinding.ActivityGameBinding
import kotlinx.coroutines.delay

class GameActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var binding: ActivityGameBinding

    val listaDeImagenes = listOf(R.drawable.error0, R.drawable.error1, R.drawable.error2, R.drawable.error3, R.drawable.error4, R.drawable.error5, R.drawable.error6, R.drawable.error7, R.drawable.error8, R.drawable.error9, R.drawable.error10)
    var numImagen = 0

    var salida = ""
    var palabraSecreta = ""
    var dificultad = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        //QUITAR BARRA DE ARRIBA
        if(supportActionBar != null){
            supportActionBar?.hide()
        }

        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        var dificultades = bundle?.getString("dificultad")

        if(dificultades == "Fácil"){
            binding.espacioLetras.text = "____"
            dificultad = "Fácil"
        }else if(dificultades == "Medio"){
            binding.espacioLetras.text = "______"
            dificultad = "Medio"
        }else if(dificultades == "Difícil"){
            binding.espacioLetras.text = "________"
            dificultad = "Difícil"
        }

        if(dificultades == "Fácil"){
            salida = "____"
            val palabras = "abad abar abes abey abia abra abre abri abro abur acal acas achi acle acme acne acre acta acto adan adia adie adio adir ador adra adre adro adul aedamás…\n" +
                    "baba babi baca bada baga bago baja baje bajo bala bale balo baña bañe baño baos bari barn baro basa base baso bata bate bati bato baul baya bayo bayumás…\n" +
                    "cabe cabo caca cace caco cada cadi cado caed caen caer caes cafe caga cago caia caid cais caja caji cajo cala cale cali calo cama cana cane cano cañamás…\n" +
                    "daba daca dada dado daga dais dala dama daña dañe daño dara dare data date dato daza deal dean deas deba debe debi debo deci dedo deis deja deje dejomás…\n" +
                    "echa eche echo ecos ecua ecuo ecus edad eden edil edra edre edro efes efod egos eira ejem ejes eles elfo ella elle ello emes emir emus ende enea eneomás…\n" +
                    "faba fabo faca fada fado fago faja faje fajo falo fama fame fano faña fañe faño fara faro fase faso fata fato favo faya feas feje fema feme femo feosmás…\n" +
                    "gafa gafe gafo gaga gago gags gais gaje gajo gala galo gama gamo gana gane gang gano gaña gañe gañi gaño gaon gaps gara garo gasa gata gato gaya gayemás…\n" +
                    "haba haca hace hada hado haga hago hala hale hall halo hamo hara hare hato havo haya hayo haza hede hedi hela hele helo heno heñi heri hice hico hielmás…\n" +
                    "iban ibas ibis ibon icen ices icho ichu icor idas idea idee idem ideo idos idus iglu ijar ileo imam iman inca inda indo inga inri inti iota ipil ipsimás…\n" +
                    "jaba jabi jaca jaco jada jade jaen jaez jais jala jale jalo jama jame jamo jane jaña jaño jara jaro jasa jase jaso jata jate jato jaul java javo jazzmás…\n" +
                    "labe laca lace laco lada lado lady lago lais laja lama lame lami lamo lana laña lañe laño lapa lapo lasa laso lata late lati lato laud lava lave lavomás…\n" +
                    "mabi maca mace mach maco maga mago maiz maja maje majo mala mali malo mama mame mamo mams mana mane mani mano maña maño mapa mapo mara maro masa masemás…\n" +
                    "naba nabi nabo naca nace naci naco nada nade nadi nado naif naja nana naos napa nasa naso nata nato nava nave nazi neas nebi nega nego neis neja nejomás…\n" +
                    "ñaja ñajo ñame ñaña ñaño ñapa ñata ñato ñeca ñeco ñipe ñire ñoca ñoco ñola ñoña ñoño ñora ñuca ñuco ñudo ñues ñuta ñuto\n" +
                    "oboe obos obra obre obro obue obus ocal ocas ocho ocia ocie ocio ocle ocra ocre odas odia odie odio odre ogro ohms oian oias oida oido oiga oigo oiramás…\n" +
                    "paca pace paci paco pacu paga pago pais paja paje pajo pala pale pali palo pana pano pañi paño papa pape papo papu para pare pari paro pasa pase pasomás…\n" +
                    "queo quia quid quif quin quio quis\n" +
                    "raba rabi rabo raco rada raed raen raer raes rafa rafe ragu raia rail rain raiz raja raje rajo rala ralo rama ramo rana rand rano raña raño rapa rapemás…\n" +
                    "sabe saca saco saga sago sagu sahs sain saja saje sajo sala sale sali salo sama sana sane sano sant saña saos sapa sapo sari sata sato sauz saxo sayamás…\n" +
                    "taba tabi tabo tabu taca tace taco tacs tael tafo taha taja taje tajo taju tala tale talo tamo tana tano taña tañe tañi taño taos tapa tape tapo taramás…\n" +
                    "ubes ubio ubis ubre uced uchu ucis ugre ulpo ulua umju unan unas unce unci unen unes unge ungi unia unid unio unir unis unja unjo unju unos unta untemás…\n" +
                    "vaca vaco vade vado vafo vaga vago vaha vahe vaho vais vale vali vals vana vano vara vare vari varo vasa vaso vate vaya vean veas vece veda vede vedomás…\n" +
                    "xeca xies xola xolo\n" +
                    "yaba yaca yace yaci yaga yago yale yana yang yapa yape yapo yapu yare yaro yate yaya yayo yebo yeco yema yero yeso yeti yeye yeyo yina yira yiro yodamás…\n" +
                    "zaca zade zafa zafe zafo zaga zala zana zapa zape zapo zata zaya zaza zazo zeda zeta zinc zoca zoco zoma zomo zona zoos zopa zope zopo zote zuas zuda"
            palabraSecreta = palabras.split(" ").random().uppercase()

        }else if(dificultades == "Medio"){
            salida = "______"
            val palabras = "ababas ababol abacal abacas abacha abache abacho abacos abadas abades abadia abadis abajad abajan abajar abajas abajen abajes abalad abalan abalar abalas abalea abalee abalen abaleo abales aballa aballe aballomás…\n" +
                    "babaca babaco babada babaza babead babean babear babeas babeen babees babeos babera babero bables babosa baboso bacada bacana bacano bacara bacera baceta bachea bachee bacheo baches bacias baciga bacilo bacinamás…\n" +
                    "cabala cabale cabalo cabaña cabare cabaza cabdal cabear cabeis cabera cabero cabete cabeza cabezo cabian cabias cabida cabido cabila cabina cabios cablea cablee cableo cables cabran cabras cabrea cabree cabreomás…\n" +
                    "dabais dables dachas dacias dacion dacios dadera dadero dadiva dadora dagame daifas daimio dajaos dalaga dalias dalind dallad dallan dallar dallas dallen dalles damero dancen dances dandis danesa dangos danicamás…\n" +
                    "ebanos ebrias ebrios eburno ecarte eccema ecepto echaba echada echado echais echara echare echase echeis echona ecloga ecoica ecoico ectima eculeo eczema edades edecan edemas edenes edicto edilas ediles editadmás…\n" +
                    "fabada fablar fablas fabril fabuco fabula fabule fabulo facata facazo facera facero faceta faceto fachad fachan fachar fachas fachea fachee fachen facheo faches fachos facial facies facion factor facula fadigamás…\n" +
                    "gabasa gabata gabato gabazo gabejo gabela gabina gabita gabote gabuzo gacela gaceta gachas gaches gachis gachon gachos gadida gadido gafaba gafada gafado gafais gafara gafare gafase gafead gafean gafear gafeasmás…\n" +
                    "habada habado habana habano habeis habian habias habida habido habita habite habito hablad hablan hablar hablas hablen hables habran habras habria haceis hacera hachad hachan hachar hachas hachea hachee hachenmás…\n" +
                    "ibamos iberas iberia iberio iberos ibices ibidem ibones icacos icarea icareo icaria icario icemos iconos icores ictiol idalia idalio ideaba ideada ideado ideais ideara ideare idease ideeis ideosa ideoso idiliomás…\n" +
                    "jabada jabado jabali jabata jabato jabeba jabeca jabega jabera jabies jabino jables jabona jabone jabono jabran jabras jabren jabres jabria jabrid jabrio jabrir jabris jabuco jabugo jacapa jacara jacaro jacenamás…\n" +
                    "labaro labeos labial labias labios labora labore laboro labrad labran labrar labras labren labres labrio labros labura labure laburo lacaba lacada lacado lacais lacara lacare lacase lacaya lacayo lacead laceanmás…\n" +
                    "mabies mables mabolo macaba macabi macaca macaco macado macais macana macara macare macase macead macean macear maceas maceen macees maceis macelo maceos macera macere macero maceta machad machan machar machasmás…\n" +
                    "nababo nababs nabato nabies nabina nabiza nablas nabori nacara naceis nacela nacera nacere nachas nachos nacian nacias nacida nacido nacion nadaba nadado nadais nadara nadare nadase nadeis nafrad nafran nafrarmás…\n" +
                    "ñacara ñagaza ñampea ñampee ñampeo ñampis ñandus ñangas ñangos ñangue ñañara ñañiga ñañigo ñaques ñarusa ñaruso ñatead ñatean ñatear ñateas ñateen ñatees ñatusa ñatuso ñeclas ñecuda ñecudo ñengas ñengos ñenguemás…\n" +
                    "obceca obceco obelos obesas obesos obices obispa obispe obispo obitos obiubi objeta objete objeto oblada oblata oblato obleas obliga obligo oblito obolos obraba obrada obrado obrais obraje obrara obrare obrasemás…\n" +
                    "pabilo pablar pabulo pacada pacado pacaes pacana pacata pacato pacaya paceis paceña paceño pacera pacere pachas pachon pachos pacian pacias pacida pacido pacion pacora pacota pactad pactan pactar pactas pactenmás…\n" +
                    "quasar quater quebra quebre quebro queche quecos quedad quedan quedar quedas queden quedes quedos quegua quejad quejan quejar quejas quejen quejes quejos quemad queman quemar quemas quemen quemes quemis quemonmás…\n" +
                    "rabada rabano rabead rabean rabear rabeas rabeen rabees rabeos rabera rabiad rabian rabiar rabias rabica rabico rabida rabido rabien rabies rabila rabile rabilo rabino rabion rabita rabiza rabona rabosa rabosomás…\n" +
                    "sabado sabalo sabana sabano sabaya sabbat sabeas sabeis sabela sabeos sabian sabias sabicu sabida sabido sabila sabina sabino sabios sablea sablee sableo sables sablon saboga sabran sabras sabria sabuco sabugomás…\n" +
                    "tabaco tabano tabeas tabefe tabica tabico tabida tabido tabies tabina tablad tablan tablao tablar tablas tablea tablee tablen tableo tables tablon tabuco tabues tabula tabule tabulo tacaco tacada tacana tacañamás…\n" +
                    "ubajay ubicad ubican ubicar ubicas ubicua ubicuo ubique ubrera ucases ufanan ufanar ufanas ufanen ufanes ufania ufanos ulagas ulalas ulanos ulcera ulcere ulcero ulemas ulluco ultima ultime ultimo ultras ultrizmás…\n" +
                    "vacaba vacada vacado vacais vacaje vacara vacare vacari vacase vaccea vacceo vaciad vacian vaciar vacias vacien vacies vacila vacile vacilo vacios vacuas vacuna vacune vacuno vacuos vadead vadean vadear vadeasmás…\n" +
                    "xecuda xecudo xilema xincas xiotes xochil\n" +
                    "yabuna yacare yacata yaceis yacera yacere yacian yacias yacido yacija yacios yagais yagual yaguar yaguas yagure yagurt yaitis yamana yamaos yambos yampas yanqui yantad yantan yantar yantas yanten yantes yapabamás…\n" +
                    "zabeca zabida zabila zaboya zaboye zaboyo zabras zabros zabuca zabuco zacate zacead zacean zacear zaceas zaceen zacees zaceos zacuto zafaba zafada zafado zafais zafara zafare zafari zafase zafeis zafero zafias"
            palabraSecreta = palabras.split(" ").random().uppercase()

        }else if(dificultades == "Difícil"){
            salida = "________"
            val palabras = "aaronica aaronico aaronita ababilla ababille ababillo ababoles abacales abaceras abaceria abaceros abachaba abachada abachado abachais abachara abachare abachase abacheis abacorad abacoran abacorar abacoras abacoren abacores abadejos abadenga abadengo abaderna abadernemás…\n" +
                    "baalitas babadero babaeros babancas babancos babeaban babeabas babeamos babeando babearan babearas babearen babeares babearia babearon babeasen babeases babeaste babeemos babelica babelico babequia babianas babianos babiecas babillas babilona babirusa babismos babonucomás…\n" +
                    "caaminis cabadura cabalaba cabalada cabalado cabalais cabalara cabalare cabalase cabaleis cabalero cabalgad cabalgan cabalgar cabalgas cabalgue cabalina cabalino caballar caballas caballea caballee caballeo caballon caballos cabangas cabañera cabañero cabargas cabarrasmás…\n" +
                    "daciones dactilar dactilos dadaismo dadaista dadivada dadivado dadivosa dadivoso daguilla daiquiri dallaban dallabas dalladas dallador dallados dallamos dallando dallaran dallaras dallaren dallares dallaria dallaron dallasen dallases dallaste dallemos dalmatas damacenamás…\n" +
                    "ebanista ebenacea ebenaceo ebionita ebonitas eboraria eborario ebriedad ebriosas ebriosos eburneas eburneos eccehomo ecdotica ecdotico eceptuar echabais echadera echadero echadiza echadizo echadora echadura echarais echareis echarian echarias echarpes echaseis ecijanasmás…\n" +
                    "fablable fabordon fabricad fabrican fabricar fabricas fabridas fabridos fabriles fabrique fabulaba fabulada fabulado fabulais fabulara fabulare fabulase fabuleis fabulosa fabuloso facciosa faccioso facedora facerias fachaban fachabas fachadas fachados fachamos fachandomás…\n" +
                    "gabachas gabachos gabardas gabarras gabarros gabinete gabletes gabonesa gacetera gacetero gachapos gachetas gachonas gachones gachumbo gachupin gacillas gaditana gaditano gaelicas gaelicos gaetanas gaetanos gafabais gafarais gafareis gafarian gafarias gafarron gafaseismás…\n" +
                    "habanera habanero habedera habedero haberada haberado haberios haberosa haberoso habiamos habidera habidero habiendo habiente habilita habilite habilito habillos habilosa habiloso habitaba habitada habitado habitais habitara habitare habitase habitats habiteis habituadmás…\n" +
                    "ibarreña ibarreño ibericas ibericos iberismo ibicenca ibicenco icastica icastico icebergs icneumon iconicas iconicos icorosas icorosos icterica icterico icterido ictineas ictineos ictioles ictiosis ideabais ideacion idealice idealiza idealizo idearais ideareis idearianmás…\n" +
                    "jabalcon jabalies jabalina jabalona jabalone jabalono jabaluna jabardea jabardee jabardeo jabardos jabegote jabelgad jabelgan jabelgar jabelgas jabelgue jabeques jabillos jabonaba jabonada jabonado jabonais jabonara jabonare jabonase jabonead jabonean jabonear jaboneasmás…\n" +
                    "labiadas labiados labiales labiosas labiosos laboraba laborada laborado laborais laborara laborare laborase laboread laborean laborear laboreas laboreen laborees laboreis laboreos laborera laborios laborosa laboroso labraban labrabas labradas labradia labradio labradormás…\n" +
                    "macabais macabeas macabeos macabies macabras macabros macacada macachin macacoas macadams macaense macaguas macanazo macanche macancoa macandon macanead macanean macanear macaneas macaneen macanees macaneos macanuda macanudo macaquea macaquee macaqueo macarais macareismás…\n" +
                    "nabateas nabateos naberias naborias nabories nacarada nacarado nacareas nacareos nacarina nacarino nacatete nacedero nacencia nacereis nacerian nacerias naciamos naciendo naciente nacieran nacieras nacieren nacieres nacieron naciesen nacieses nacional naciones nacritasmás…\n" +
                    "ñacurutu ñamerias ñampeaba ñampeada ñampeado ñampeais ñampeara ñampeare ñampease ñampeeis ñandubay ñandutis ñangaras ñangazos ñangotan ñangotar ñangotas ñangoten ñangotes ñapangas ñapangos ñapindas ñateaban ñateabas ñateadas ñateados ñateamos ñateando ñatearan ñatearasmás…\n" +
                    "obcecaba obcecada obcecado obcecais obcecara obcecare obcecase obcequen obceques obedeced obedecen obedecer obedeces obedecia obedecio obedezca obedezco obelisco obenques obertura obesidad obispaba obispado obispais obispara obispare obispase obispeis obiubies objecionmás…\n" +
                    "pabellon pabilosa pabiloso pacciona paccione pacciono pacedera pacedero pacedura pacenses pacereis pacerian pacerias pachacha pachacho pachanga pacharan pachecos pachiche pachichi pachilla pachocha pacholas pacholis pachonas pachones pachorra pachucas pachucha pachuchomás…\n" +
                    "quebraba quebrada quebrado quebrais quebraja quebraje quebrajo quebrara quebrare quebrase quebraza quebreis quechuas quedaban quedabas quedadas quedados quedamos quedando quedaran quedaras quedaren quedares quedaria quedaron quedasen quedases quedaste quedemos quehacermás…\n" +
                    "rabalera rabalero rabanera rabanero rabaniza rabeaban rabeabas rabeamos rabeando rabearan rabearas rabearen rabeares rabearia rabearon rabeasen rabeases rabeaste rabeemos rabelero rabiaban rabiabas rabiadas rabiamos rabiando rabiaran rabiaras rabiaren rabiares rabiariamás…\n" +
                    "sabadeña sabadeño sabalera sabalero sabanazo sabanead sabanean sabanear sabaneas sabaneen sabanees sabaneos sabanera sabanero sabaneta sabatica sabatice sabatico sabatina sabatino sabatiza sabatizo sabedora sabeismo sabelica sabelico sabencia sabiamos sabicues sabidoramás…\n" +
                    "tabachin tabacosa tabacoso tabaibal tabaibas tabalada tabalead tabalean tabalear tabaleas tabaleen tabalees tabaleos tabanazo tabancos tabanera tabanque tabaques tabardos tabarras tabarros tabascos tabelion tabellad tabellan tabellar tabellas tabellen tabelles tabernasmás…\n" +
                    "ubajayes uberrima uberrimo ubetense ubicaban ubicabas ubicadas ubicados ubicamos ubicando ubicaran ubicaras ubicaren ubicares ubicaria ubicaron ubicasen ubicases ubicaste ubiqueis ucranias ucranios ucronias ucronica ucronico udometro ufanaban ufanabas ufanamos ufanandomás…\n" +
                    "vacabais vacabuey vacacion vacancia vacantes vacarais vacareis vacarian vacarias vacaries vacaseis vacatura vaciaban vaciabas vaciadas vaciador vaciados vaciamos vaciando vaciante vaciaran vaciaras vaciaren vaciares vaciaria vaciaron vaciasen vaciases vaciaste vaciedadmás…\n" +
                    "xantomas xenismos xenofoba xenofobo xerofila xerofilo xerofita xerofito xeroteca xicaques xifoidea xifoideo xifoides xilofaga xilofago xilofono xilotila xochiles xocoyote\n" +
                    "yaacabos yacentes yacereis yacerian yacerias yaciamos yaciendo yaciente yacieran yacieras yacieren yacieres yacieron yaciesen yacieses yacturas yacumeña yacumeño yagrumas yagrumos yaguales yaguanes yaguares yaguasas yaicuaje yambicas yambicos yanacona yanguesa yanillasmás…\n" +
                    "zabazala zabordad zabordan zabordar zabordas zaborden zabordes zabordos zaborras zaborros zaboyaba zaboyada zaboyado zaboyais zaboyara zaboyare zaboyase zaboyeis zabucaba zabucada zabucado zabucais zabucara zabucare zabucase zabullan zabullas zabullen zabulles zabullia"
            palabraSecreta = palabras.split(" ").random().uppercase()
        }

        binding.aBoton.setOnClickListener(this)
        binding.bBoton.setOnClickListener(this)
        binding.cBoton.setOnClickListener(this)
        binding.dBoton.setOnClickListener(this)
        binding.eBoton.setOnClickListener(this)
        binding.fBoton.setOnClickListener(this)
        binding.gBoton.setOnClickListener(this)
        binding.hBoton.setOnClickListener(this)
        binding.iBoton.setOnClickListener(this)
        binding.jBoton.setOnClickListener(this)
        binding.kBoton.setOnClickListener(this)
        binding.lBoton.setOnClickListener(this)
        binding.mBoton.setOnClickListener(this)
        binding.nBoton.setOnClickListener(this)
        binding.enieBoton.setOnClickListener(this)
        binding.oBoton.setOnClickListener(this)
        binding.pBoton.setOnClickListener(this)
        binding.qBoton.setOnClickListener(this)
        binding.rBoton.setOnClickListener(this)
        binding.sBoton.setOnClickListener(this)
        binding.tBoton.setOnClickListener(this)
        binding.uBoton.setOnClickListener(this)
        binding.vBoton.setOnClickListener(this)
        binding.wBoton.setOnClickListener(this)
        binding.xBoton.setOnClickListener(this)
        binding.yBoton.setOnClickListener(this)
        binding.zBoton.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        Toast.makeText(this, "alphabet[itemPosition].toString", Toast.LENGTH_SHORT)

        val boton = p0 as Button

        if(dificultad == "Fácil"){
            if(boton.text.toString() in palabraSecreta){
                var nuevaSalida = ""
                binding.espacioLetras.text = binding.aBoton.text.toString()
                for(i in 0..3){
                    if(salida[i].toString() == "_" ){
                        if(boton.text.toString() == palabraSecreta[i].toString()){
                            nuevaSalida +=boton.text.toString()
                        }else{
                            nuevaSalida += "_"
                        }
                    }else{
                        nuevaSalida += salida[i]
                    }
                }
                salida = nuevaSalida
            }else{
                numImagen++
                binding.imagenesHangman.setImageResource(listaDeImagenes[numImagen])
            }

            boton.visibility = View.GONE
            binding.espacioLetras.text = salida

        }else if(dificultad == "Medio"){
            if(boton.text.toString() in palabraSecreta){
                var nuevaSalida = ""
                binding.espacioLetras.text = binding.aBoton.text.toString()
                for(i in 0..5){
                    if(salida[i].toString() == "_" ){
                        if(boton.text.toString() == palabraSecreta[i].toString()){
                            nuevaSalida +=boton.text.toString()
                        }else{
                            nuevaSalida += "_"
                        }
                    }else{
                        nuevaSalida += salida[i]
                    }
                }
                salida = nuevaSalida
            }else{
                numImagen++
                binding.imagenesHangman.setImageResource(listaDeImagenes[numImagen])
            }

            boton.visibility = View.GONE
            binding.espacioLetras.text = salida
        }else if(dificultad == "Difícil"){
            if(boton.text.toString() in palabraSecreta){
                var nuevaSalida = ""
                binding.espacioLetras.text = binding.aBoton.text.toString()
                for(i in 0..7){
                    if(salida[i].toString() == "_" ){
                        if(boton.text.toString() == palabraSecreta[i].toString()){
                            nuevaSalida +=boton.text.toString()
                        }else{
                            nuevaSalida += "_"
                        }
                    }else{
                        nuevaSalida += salida[i]
                    }
                }
                salida = nuevaSalida
            }else{
                numImagen++
                binding.imagenesHangman.setImageResource(listaDeImagenes[numImagen])
            }

            boton.visibility = View.GONE
            binding.espacioLetras.text = salida
        }


        //PIERDE
        if(numImagen >= 10){
            val intent = Intent(this, LoseActivity::class.java)
            val ganadoOperdido = "perdido"
            intent.putExtra("ganadoOperdido", ganadoOperdido)
            intent.putExtra("palabraSecreta", palabraSecreta)
            startActivity(intent)
        }
        //GANA
        if(salida == palabraSecreta){
            val intent = Intent(this, LoseActivity::class.java)
            val ganadoOperdido = "ganado"
            intent.putExtra("ganadoOperdido", ganadoOperdido)
            intent.putExtra("palabraSecreta", palabraSecreta)
            startActivity(intent)
        }

    }
}